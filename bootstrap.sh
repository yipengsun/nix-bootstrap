#!/bin/sh
# vim: set noexpandtab:

set -e
set -x

# install path
NIX_PREFIX=`readlink -f $HOME` # path to nix store must not contain symlinks
MYTMP=/tmp/nix-boot-`whoami`
NUM_THREADS=4

export PATH=/bin:/usr/bin:/usr/sbin:/sbin
export LD_LIBRARY_PATH=""

rm -rf $MYTMP
mkdir $MYTMP
pushd $MYTMP

export PATH=$MYTMP/bin:$PATH
export LD_LIBRARY_PATH=$MYTMP/lib64:$MYTMP/lib:$LD_LIBRARY_PATH

function mmi () {
	make -j $NUM_THREADS 1> /dev/null
	make install 1> /dev/null
}

GCC_VERSION=5.5.0
BROTLI_VERSION=1.0.7
BOOST_VERSION=1.68.0
BOOST_DIR_VERSION=`echo $BOOST_VERSION | sed 's/\./_/g'`
NIX_VERSION=2.1.3

wget https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.xz
wget https://github.com/google/brotli/archive/v$BROTLI_VERSION.tar.gz
wget https://dl.bintray.com/boostorg/release/$BOOST_VERSION/source/boost_$BOOST_DIR_VERSION.tar.bz2
wget https://nixos.org/releases/nix/nix-$NIX_VERSION/nix-$NIX_VERSION.tar.xz

tar Jxf gcc-$GCC_VERSION.tar.xz
tar zxf v$BROTLI_VERSION.tar.gz
tar jxf boost_$BOOST_DIR_VERSION.tar.bz2
tar Jxf nix-$NIX_VERSION.tar.xz

# TODO add detection for when gcc is already new enough
pushd gcc-$GCC_VERSION
./contrib/download_prerequisites
popd
rm -rf objdir || true
mkdir -p objdir
pushd objdir
$PWD/../gcc-$GCC_VERSION/configure --prefix=$MYTMP --enable-languages=c,c++ --disable-multilib --disable-bootstrap 1>/dev/null 2>/dev/null
mmi
popd

pushd brotli-$BROTLI_VERSION
./configure-cmake --prefix=$MYTMP 1>/dev/null 2>/dev/null
mmi
popd

pushd boost_$BOOST_DIR_VERSION
./bootstrap.sh --with-libraries=context --prefix=$MYTMP 1>/dev/null 2>/dev/null
./b2 install 1>/dev/null 2>/dev/null
popd

pushd nix-$NIX_VERSION
export PKG_CONFIG_PATH=$MYTMP/lib/pkgconfig
./configure \
	--disable-seccomp-sandboxing \
	--prefix=$MYTMP \
	--with-store-dir=$NIX_PREFIX/nix/store \
	--localstatedir=$NIX_PREFIX/nix/var
mmi
popd

if [ ! -d ~/nixpkgs ]; then
	git clone -b release-18.09 https://github.com/NixOS/nixpkgs.git ~/nixpkgs
fi

if [ ! -L ~/.nix-profile ]; then
	ln -s $NIX_PREFIX/nix/var/nix/profiles/default ~/.nix-profile
fi

if [ ! -d ~/.nixpkgs ]; then
	mkdir ~/.nixpkgs || true
	cat <<EOF > ~/.nixpkgs/config.nix
pkgs:
{
  packageOverrides = pkgs: {
    nix = pkgs.nix.override {
      storeDir = "$NIX_PREFIX/nix/store";
      stateDir = "$NIX_PREFIX/nix/var";
    };
  };
}
EOF
fi

# use nix to bootstrap stdenv and install proper nix
nix-env -Q -j $NUM_THREADS -iA nix -f ~/nixpkgs

rm -rf $MYTMP
set +x

cat <<EOF


============================================

Add following to your shell rc file:

export NIX_PATH=nixpkgs=\$HOME/nixpkgs
source ~/.nix-profile/etc/profile.d/nix.sh

============================================

EOF
